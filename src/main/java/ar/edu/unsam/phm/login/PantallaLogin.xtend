package ar.edu.unsam.phm.login

import ar.edu.unsam.phm.pantallaPrincipal.PantallaPrincipal
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.PasswordField
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.windows.SimpleWindow
import org.uqbar.arena.windows.WindowOwner

class PantallaLogin extends SimpleWindow<PantallaLoginApplicationModel> {
	
	new(WindowOwner parent){
		super (parent, new PantallaLoginApplicationModel)
	}
	
	override protected createFormPanel(Panel mainPanel) {
		
		title = "Ingreso al sistema"
		mainPanel.layout = new VerticalLayout
		
		new Label(mainPanel).text = "User Name"
		new TextBox(mainPanel) => [
			bindValueToProperty("nick")
		]
		
		new Label(mainPanel).text = "Password"
		new PasswordField(mainPanel) => [
			bindValueToProperty("contrasena")
		]

		val panelBotonera = new Panel(mainPanel).layout = new HorizontalLayout

		new Button(panelBotonera) => [
			caption = "Login"
			onClick[ | new PantallaPrincipal(this, modelObject.logear()).open]
		] 

		new Button(panelBotonera) => [
			caption = "Cancelar"
			onClick[ | this.close()]
		]
		
	}
	
	override protected addActions(Panel actionsPanel) {
	}
	
}