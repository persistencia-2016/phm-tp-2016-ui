package ar.edu.unsam.phm.login

import ar.edu.unsam.phm.domain.Usuario
import ar.edu.unsam.phm.repo.Repositorio
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class PantallaLoginApplicationModel {

	String nick
	String contrasena
	
	public def Usuario logear() {
		Repositorio.getInstance().getUnUsuario(nick, contrasena)
	}
	
}