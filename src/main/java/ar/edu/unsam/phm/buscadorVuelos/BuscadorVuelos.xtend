package ar.edu.unsam.phm.buscadorVuelos

import ar.edu.unsam.phm.domain.Usuario
import ar.edu.unsam.phm.domain.Vuelo
import ar.edu.unsam.phm.filter.FechaFilter
import ar.edu.unsam.phm.reservaAsiento.ReservaAsiento
import org.uqbar.arena.aop.windows.TransactionalDialog
import org.uqbar.arena.bindings.NotNullObservable
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.Selector
import org.uqbar.arena.widgets.TextBox
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.windows.WindowOwner

class BuscadorVuelos extends TransactionalDialog<BuscadorVuelosApplicationModel>{
	
	new(WindowOwner owner, Usuario usuario) {
		super(owner, new BuscadorVuelosApplicationModel(usuario))
	}
	

override protected createFormPanel(Panel mainPanel) {
		val elementSelected = new NotNullObservable("vueloSeleccionado")
		title = "BUSCADOR DE VUELOS"
		mainPanel.layout = new VerticalLayout
		
		val PanelTitulosConsulta = new Panel(mainPanel)
		PanelTitulosConsulta.layout = new ColumnLayout(5)
		
		
		new Label(PanelTitulosConsulta).text = "Origen"
		new Label(PanelTitulosConsulta).text = "Destino"
		new Label(PanelTitulosConsulta).text = "Fecha desde"
		new Label(PanelTitulosConsulta).text = "Fecha hasta"
		new Label(PanelTitulosConsulta).text = "$ Maximo"
		
		val PanelCamposConsulta = new Panel(mainPanel)
		PanelCamposConsulta.layout = new ColumnLayout(5)
		
		new Selector<String>(PanelTitulosConsulta) => [
			bindItemsToProperty("ciudades")
			bindValueToProperty("busqueda.origen")
		]
		
		new Selector<String>(PanelTitulosConsulta) => [
			bindItemsToProperty("ciudades")
			bindValueToProperty("busqueda.destino")
		]
		
		new TextBox(PanelTitulosConsulta) => [
			bindValueToProperty("busqueda.fechaS")
			withFilter(new FechaFilter)
		]
		
		
		new TextBox(PanelTitulosConsulta) => [
			bindValueToProperty("busqueda.fechaA")
			withFilter(new FechaFilter)
		]
			
		new TextBox(PanelTitulosConsulta) => [
			bindValueToProperty("busqueda.precioMaximo")
		]
			
		
		val PanelSeparadorConsultaResultados = new Panel(mainPanel)
		PanelSeparadorConsultaResultados.layout = new HorizontalLayout
		
		new Label(PanelSeparadorConsultaResultados).text = "Vuelos"
		
		new Button(mainPanel) => [
			caption = "Buscar"
			onClick [ | modelObject.buscar()
			]
		]
		
		grillaReservas(mainPanel)
		
		new Button(mainPanel) => [
			caption = "Reservar"
			onClick [ | this.verVuelo()]
			bindEnabled(elementSelected)
		]
	
	}
		def grillaReservas(Panel mainPanel) {

		val grilla = new Table(mainPanel, typeof(Vuelo)) => [
			width = 400
			height = 2000
			bindItemsToProperty("vuelos")
			bindValueToProperty("vueloSeleccionado")
		]
		
		new Column<Vuelo>(grilla) => [
			fixedSize = 200
			title = "Origen"
			bindContentsToProperty("origen")
		]

		new Column<Vuelo>(grilla) => [
			fixedSize = 200
			title = "Destino"
			bindContentsToProperty("destino")
		]

		new Column<Vuelo>(grilla) => [
			fixedSize = 200
			title = "Salida"
			bindContentsToProperty("fechaSalidaToStr")
		]

		new Column<Vuelo>(grilla) => [
			fixedSize = 200
			title = "Llegada"
			bindContentsToProperty("fechaArriboToStr")
		]

		new Column<Vuelo>(grilla) => [
			fixedSize = 200
			title = "Tramos"
			bindContentsToProperty("cantidadEscalas")
		]
		
		new Column<Vuelo>(grilla) => [
			fixedSize = 200
			title = "Cant. de asientos libres"
			bindContentsToProperty("cantidadDeAsientosLibres")
		]
	}
	
	def verVuelo(){
		
		(new ReservaAsiento(this, modelObject.usuario, modelObject.vueloSeleccionado)).open()
	}
}