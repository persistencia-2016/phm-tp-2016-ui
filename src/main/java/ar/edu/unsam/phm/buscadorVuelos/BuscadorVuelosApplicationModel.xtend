package ar.edu.unsam.phm.buscadorVuelos

import ar.edu.unsam.phm.domain.BusquedaVuelo
import ar.edu.unsam.phm.domain.LogConsulta
import ar.edu.unsam.phm.domain.Usuario
import ar.edu.unsam.phm.domain.Vuelo
import ar.edu.unsam.phm.repo.Repositorio
import ar.edu.unsam.phm.repo.RepositorioLogs
import java.util.ArrayList
import java.util.Collection
import java.util.Date
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Observable
@Accessors
class BuscadorVuelosApplicationModel {
	
	RepositorioLogs repositorioLogs
	Vuelo vueloSeleccionado
	Usuario usuario
	Collection<String> ciudades
	BusquedaVuelo busqueda = new BusquedaVuelo
	List<Vuelo> vuelos = new ArrayList<Vuelo>
	
	new(Usuario usuarioParametro) {
		usuario = usuarioParametro
		ciudades = Repositorio.getInstance().getCiudades()
		repositorioLogs = RepositorioLogs.getInstance()
	}
	
	def buscar(){
		vuelos = Repositorio.getInstance().BuscadorVuelos(this.filtros())
		repositorioLogs.agregarConsulta(new LogConsulta(usuario,busqueda.devolverParametros(),vuelos))
	}
	
		
	
	def filtros(){
		busqueda.getFiltros()
	}
}