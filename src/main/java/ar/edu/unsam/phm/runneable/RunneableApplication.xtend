package ar.edu.unsam.phm.runneable

import org.uqbar.arena.Application
import org.uqbar.arena.windows.Window
import ar.edu.unsam.phm.login.PantallaLogin

class RunneableApplication extends Application{

	static def void main(String[] args) { 
		new RunneableApplication().start()
	}

	override protected Window<?> createMainWindow() {
		return new PantallaLogin(this)
	}
}