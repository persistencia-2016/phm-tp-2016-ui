package ar.edu.unsam.phm.filter

import org.uqbar.arena.filters.TextFilter
import org.uqbar.arena.widgets.TextInputEvent

class LetrasFilter implements TextFilter {

	override accept(TextInputEvent event) {
		event.potentialTextResult.matches("[a-zA-Z ]*")
	}		

}