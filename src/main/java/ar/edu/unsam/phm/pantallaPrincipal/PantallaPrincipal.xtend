package ar.edu.unsam.phm.pantallaPrincipal

import ar.edu.unsam.phm.buscadorVuelos.BuscadorVuelos
import ar.edu.unsam.phm.domain.Reserva
import ar.edu.unsam.phm.domain.Usuario
import org.uqbar.arena.aop.windows.TransactionalDialog
import org.uqbar.arena.bindings.NotNullObservable
import org.uqbar.arena.layout.HorizontalLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.windows.WindowOwner

class PantallaPrincipal extends TransactionalDialog<PantallaPrincipalApplicationModel> {

	new(WindowOwner owner, Usuario usuario) {
		super(owner, new PantallaPrincipalApplicationModel(usuario))
	}

	override protected createFormPanel(Panel mainPanel) {

		val elementSelected = new NotNullObservable("reservaSeleccionada")

		title = "Pantalla Principal"
		mainPanel.layout = new VerticalLayout
		
		new Label(mainPanel).bindValueToProperty("usuario.nombre")
		
		new Label(mainPanel).bindValueToProperty("mensaje")
		grillaReservas(mainPanel)
		
		val PanelBotonera = new Panel(mainPanel)
		PanelBotonera.layout = new HorizontalLayout
		
		new Button(PanelBotonera) => [
			caption = "Cancelar Reserva"
			onClick [ | 
															
						modelObject.cancelarReserva()
			]
			bindEnabled(elementSelected)
		]
		
		
		new Button(PanelBotonera) => [
			caption = "Busqueda de vuelos"
			onClick[ | this.busquedaVuelos() ]
		]		

		new Button(PanelBotonera) => [
			caption = "Log de consultas hechas"
			//onClick [ | this.mostrarLogs() ]	
		]	
			
	}

	def grillaReservas(Panel mainPanel) {

		val grilla = new Table(mainPanel, typeof(Reserva)) => [
			width = 400
			height = 2000
			bindItemsToProperty("usuario.reservas")
			bindValueToProperty("reservaSeleccionada")
		]
		
		new Column<Reserva>(grilla) => [
			fixedSize = 200
			title = "Origen"
			bindContentsToProperty("vuelo.origen")
		]

		new Column<Reserva>(grilla) => [
			fixedSize = 200
			title = "Destino"
			bindContentsToProperty("vuelo.destino")
		]

		new Column<Reserva>(grilla) => [
			fixedSize = 200
			title = "Salida"
			bindContentsToProperty("vuelo.fechaSalidaToStr")
		]

		new Column<Reserva>(grilla) => [
			fixedSize = 200
			title = "Llegada"
			bindContentsToProperty("vuelo.fechaArriboToStr")
		]

		new Column<Reserva>(grilla) => [
			fixedSize = 200
			title = "Tramos"
			bindContentsToProperty("vuelo.cantidadEscalas")
		]

		new Column<Reserva>(grilla) => [
			fixedSize = 200
			title = "Asiento reservado"
			bindContentsToProperty("asientoReservado")
		]

	}
	
	def void cancelarReserva(){
			modelObject.cancelarReserva()
	}
	
	def void busquedaVuelos(){
		
		(new BuscadorVuelos(this, modelObject.usuario)).open()
	}
}