package ar.edu.unsam.phm.pantallaPrincipal

import ar.edu.unsam.phm.domain.Reserva
import ar.edu.unsam.phm.domain.Usuario
import java.util.ArrayList
import java.util.List
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class PantallaPrincipalApplicationModel {
	
	String mensaje
	Usuario usuario
	List<Reserva> reservas = new ArrayList<Reserva>
	Reserva reservaSeleccionada

	new(Usuario unUsuario){
		usuario = unUsuario	
		mensaje = "Reservas Efectuadas"
	}
	
	def void cancelarReserva(){
		usuario.cancelarReserva(reservaSeleccionada.vuelo, reservaSeleccionada.asiento)
	}
	
}