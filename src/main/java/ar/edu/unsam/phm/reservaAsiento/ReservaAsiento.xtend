package ar.edu.unsam.phm.reservaAsiento

import ar.edu.unsam.phm.domain.Escala
import ar.edu.unsam.phm.domain.Usuario
import ar.edu.unsam.phm.domain.Vuelo
import java.awt.Color
import org.uqbar.arena.aop.windows.TransactionalDialog
import org.uqbar.arena.bindings.NotNullObservable
import org.uqbar.arena.layout.ColumnLayout
import org.uqbar.arena.layout.VerticalLayout
import org.uqbar.arena.widgets.Button
import org.uqbar.arena.widgets.Label
import org.uqbar.arena.widgets.Panel
import org.uqbar.arena.widgets.tables.Column
import org.uqbar.arena.widgets.tables.Table
import org.uqbar.arena.windows.WindowOwner

class ReservaAsiento extends TransactionalDialog<ReservaAsientoApplicationModel>{
	
	new(WindowOwner owner, Usuario usuario, Vuelo vueloSeleccionado) {
		super(owner, new ReservaAsientoApplicationModel(usuario,vueloSeleccionado))
	}
	
	override protected createFormPanel(Panel mainPanel) {

		val elementSelected = new NotNullObservable("asientoSeleccionado")

		title = "Reserva de Asiento"
		mainPanel.layout = new VerticalLayout
		
		val PanelInformaciónSuperior = new Panel(mainPanel)
		PanelInformaciónSuperior.layout = new ColumnLayout(4)
		
		val PanelInformaciónInferior = new Panel(mainPanel)
		PanelInformaciónInferior.layout = new ColumnLayout(4)
		
		new Label(PanelInformaciónSuperior).bindValueToProperty("vuelo.origen")
		
		new Label(PanelInformaciónSuperior).text = "   ------->"
		
		new Label(PanelInformaciónSuperior).bindValueToProperty("vuelo.destino")
		
		new Label(PanelInformaciónSuperior).text = "Usuario"

		new Label(PanelInformaciónInferior).bindValueToProperty("vuelo.fechaSalidaToStr")
		
		new Label(PanelInformaciónInferior).text = "   ------->"
		
		new Label(PanelInformaciónInferior).bindValueToProperty("vuelo.fechaArriboToStr")
		
		new Label(PanelInformaciónInferior).bindValueToProperty("usuario.nombre")
		
		new Label(mainPanel).text = ""
		
		val PanelInformacionTramoAerolinea = new Panel(mainPanel)
		PanelInformacionTramoAerolinea.layout = new ColumnLayout(2)
		
		val panelIzquierdo = new Panel(PanelInformacionTramoAerolinea)
		val panelDerecho = new Panel(PanelInformacionTramoAerolinea)
		
		new Label(panelIzquierdo).text = "Tramos"
		
		new Label(panelDerecho).text = "Aerolinea"
		
		grillaTramos(panelIzquierdo)
		
		new Label(panelDerecho).bindValueToProperty("vuelo.aerolinea")
		
		new Label(mainPanel).text = "			Asientos"
		
		val PanelAsientos = new Panel(mainPanel)
		PanelAsientos.layout = new ColumnLayout(3)
		
		this.modelObject.vuelo.asientos.forEach [ asiento |
			new Button(PanelAsientos) => [ 
				caption = asiento.devolvermeComoString()
				onClick [| modelObject.asignarAsientoSeleccionado(asiento)]
				if (asiento.noEstoyReservado) {
       				background = Color.GREEN
      				} else {
       				background = Color.RED
      			}
			]
		]	
		
		new Label(PanelAsientos).text = "Asiento elegido"
		
		new Label(PanelAsientos).text = "Monto a pagar"
		
		new Button(PanelAsientos) => [
			caption = "Reservar"
			onClick [ | modelObject.reservar()
						this.close()
			]	
			bindEnabled(elementSelected)
		]
				
		new Label(PanelAsientos).bindValueToProperty("asientoSeleccionado.devolvermeComoString")
		
		new Label(PanelAsientos).bindValueToProperty("precio")
		
	}
	
	def grillaTramos (Panel panel){
		
		val grilla = new Table(panel, typeof(Escala)) => [
			width = 100
			height = 2000
			bindItemsToProperty("vuelo.escalas")
		]
		
		new Column<Escala>(grilla) => [
			fixedSize = 50
			title = "Destino Intermedio"
			bindContentsToProperty("destinoIntermedio")
		]

		new Column<Escala>(grilla) => [
			fixedSize = 50
			title = "Llegada"
			bindContentsToProperty("horaLlegadaToStr")
		]
	}
	
}