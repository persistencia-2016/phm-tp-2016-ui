package ar.edu.unsam.phm.reservaAsiento

import ar.edu.unsam.phm.domain.Asiento
import ar.edu.unsam.phm.domain.Usuario
import ar.edu.unsam.phm.domain.Vuelo
import org.eclipse.xtend.lib.annotations.Accessors
import org.uqbar.commons.utils.Observable

@Accessors
@Observable
class ReservaAsientoApplicationModel {
	
	Usuario usuario
	Vuelo vuelo
	Asiento asientoSeleccionado
	Double precio 
	
	new(Usuario unUsuario,Vuelo vueloSeleccionado){
		usuario = unUsuario	
		vuelo = vueloSeleccionado
	}
	
	def asignarAsientoSeleccionado(Asiento asiento){
		asientoSeleccionado = asiento
		precio = asientoSeleccionado.dameTuPrecio(vuelo.fechaSalida)
	}
	
	def reservar(){
		usuario.reservarAsiento(vuelo,asientoSeleccionado)
	}
	
}